<?php
/**
 * User: David Fichtenbaum
 * Date: 21/04/16
 * Time: 09:45
 */
use summa\app\App;

define('ROOT', __DIR__);

require_once ROOT . '/../vendor/autoload.php';

$app = new App();

$app->run();
