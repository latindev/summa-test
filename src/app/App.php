<?php
/**
 * User: David Fichtenbaum
 * Date: 21/04/16
 * Time: 14:36
 */

namespace summa\app;
use summa\controller;
use summa\exception\ExceptionNotFound;


class App
{
    private $segments;
    private $controller;
    private $action;

    /**
     * App constructor.
     */
    public function __construct()
    {
        $this->init();
    }

    /**
     * @return mixed
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    protected function init()
    {
        $this->setSegments();
        $this->setController();
        $this->setAction();
    }

    /**
     *
     */
    private function setSegments()
    {
        if (isset($_SERVER['REQUEST_URI']) && count($_SERVER['REQUEST_URI']) > 0 ) {
            $temp_segments = explode('/', $_SERVER['REQUEST_URI']);
            $segments = [];
            foreach ($temp_segments as $segment) {
                $pos = strpos($segment, '?');

                if ($pos >= 0) {
                    $lastSegment = explode('?', $segment);
                    $segment = $lastSegment[0];
                }
                if (!empty($segment)) {
                    $segments[] = $segment;
                }
            }

            $this->segments = $segments;
        }
    }

    private function setController()
    {
        if (isset($this->segments[0]) && !empty($this->segments[0])) {
            $this->controller = trim($this->segments[0]);
            $this->controller = ucfirst(strtolower($this->controller)) . 'Controller';
        } else {
            $this->controller = 'EmpresaController';
        }
    }

    private function setAction()
    {
        if (isset($this->segments[1]) && !empty($this->segments[1])) {
            $this->action = trim($this->segments[1]);
        } else {
          $this->action = 'index';
        }
    }

    public function run()
    {
        if (!isset($_SERVER['REDIRECT_STATUS']) || $_SERVER['REDIRECT_STATUS'] == 200)
        {

            $className = 'summa\controller\\' . $this->getController();
            $method = $this->getAction();
            try {

                try {

                    $business =  new $className;
                    $business->$method();
                }
                catch (ExceptionNotFound $ex) {
                    echo $ex->getMessage();
                }

            } catch (\Exception $ex) {
                echo "Excepcion no capturada:<br>" . $ex->getMessage();
            }
        }

    }

    /**
     * @return mixed
     */
    public function getSegments()
    {
        return $this->segments;
    }

}