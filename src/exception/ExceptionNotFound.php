<?php
/**
 * User: David Fichtenbaum
 * Date: 22/04/16
 * Time: 13:47
 */

namespace summa\exception;


use Exception;

class ExceptionNotFound extends Exception
{
    /**
     * ExceptionNotFound constructor.
     *
     * @param string $message nombre de la clase/controlador
     * @param int $code
     * @param Exception $previous
     */
    public function __construct($message, $code = 0, Exception $previous = null)
    {
        $message = 'La acción ' . $message . ' no se logro ejecutar';
        parent::__construct($message, $code, $previous);
    }
    
}