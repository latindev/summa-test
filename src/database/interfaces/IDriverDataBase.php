<?php
/**
 * User: David Fichtenbaum
 * Date: 21/04/16
 * Time: 14:46
 */

namespace summa\database\interfaces;

interface IDriverDataBase
{

    public function insert($sql, $params = [], $options = []);

    public function update($sql, $params = [], $options = []);

    public function delete($sql, $params = [], $options = []);

    public function query($sql, $params = [], $options = []);
    
}