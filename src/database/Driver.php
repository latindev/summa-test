<?php
/**
 * User: David Fichtenbaum
 * Date: 21/04/16
 * Time: 14:45
 */

namespace summa\database;


use summa\database\interfaces\IDriverDataBase;

abstract class Driver implements IDriverDataBase
{
    /**
     * @var \PDO $db
     */
    static protected $db = null;
    protected $dsn;
    protected $user;
    protected $password;
    protected $db_name;
    protected $hostname;

    public function __construct($user, $password, $db_name, $hostname)
    {
        $this->user = $user;
        $this->password = $password;
        $this->db_name = $db_name;
        $this->hostname = $hostname;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getDbName()
    {
        return $this->db_name;
    }

    /**
     * @param mixed $db_name
     */
    public function setDbName($db_name)
    {
        $this->db_name = $db_name;
    }

    /**
     * @return mixed
     */
    public function getHostname()
    {
        return $this->hostname;
    }

    /**
     * @param mixed $hostname
     */
    public function setHostname($hostname)
    {
        $this->hostname = $hostname;
    }


}