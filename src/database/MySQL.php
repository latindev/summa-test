<?php
/**
 * User: David Fichtenbaum
 * Date: 21/04/16
 * Time: 14:44
 */

namespace summa\database;


use summa\database\interfaces\IDriverDataBase;
use PDO;

class MySQL extends Driver implements IDriverDataBase
{
    /**
     * MySQL constructor.
     *
     * @param $user
     * @param $password
     * @param $db_name
     * @param $hostname
     * @param array $options
     */
    public function __construct($user, $password, $db_name, $hostname, $options = [])
    {
        parent::__construct($user, $password, $db_name, $hostname);
        $this->dsn = 'mysql:host=' . $this->hostname . ';dbname=' . $this->db_name;

        try {

            if (!self::$db instanceof PDO) {
                self::$db = new PDO($this->dsn, $this->user, $this->password, $options);
            }
        }
        catch(\PDOException $ex) {
            throw new \PDOException('No se pudo conectar a la base de datos. Revise el archivo Config.php', 0, $ex);
        }
    }

    /**
     * @param $sql
     * @param array $params
     * @param array $options
     * @throws \Exception
     */
    public function insert($sql, $params = [], $options = [])
    {

        try {
            $sth = self::$db->prepare($sql);

            $sth->execute($params);
        } catch (\PDOException $ex) {
            throw new \Exception('No se pudo agregar el registro');
        }
    }

    public function update($sql, $params = [], $options = [])
    {
        // TODO: Implement update() method.
    }

    public function delete($sql, $params = [], $options = [])
    {
        // TODO: Implement delete() method.
    }


    /**
     * @param $sql
     * @param array $params
     * @param array $options
     * @return array
     * @throws \Exception
     */
    public function query($sql, $params = [], $options = [])
    {
        try {
            $sth = self::$db->prepare($sql);
            $sth->execute($params);
            //print_r($sth);
            return $sth->fetchAll();
        } catch (\PDOException $ex) {
            throw new \Exception("No se pudieron obtener los registros");
        }
    }
}