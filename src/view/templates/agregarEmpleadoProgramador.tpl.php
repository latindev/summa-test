<?php
use summa\entity\Programador;
use summa\entity\Empresa;
/** @var Programador $data['empleado'] */
/** @var Empresa $empresa */
?>
<h1>Agregar empleado</h1>
<?php if (isset($data['errores']) && count($data['errores'])): ?>
    <?php foreach ($data['errores'] as $error): ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $error; ?>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
<?php if (isset($data['success'])): ?>
        <div class="alert alert-success" role="alert">
            El empleado se ha dado de alta con éxito
        </div>
<?php endif; ?>
<form method="post">
    <input type="hidden" name="id_empresa" value="<?php echo $data['empresa']->getId();?>" >
    <div class="form-group">
        <label for="exampleInputEmail1">Nombre</label>
        <input type="text" name="nombre" value="<?php echo $data['empleado']->getNombre();?>" class="form-control" id="exampleInputEmail1" placeholder="Nombre">
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Apellido</label>
        <input type="text" name="apellido" value="<?php echo $data['empleado']->getApellido();?>" class="form-control" id="exampleInputEmail1" placeholder="Apellido">
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Edad</label>
        <input  type="text" name="edad" class="form-control" id="exampleInputEmail1" placeholder="Edad" value="<?php echo $data['empleado']->getEdad();?>">
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Especialidad</label>
        <select class="form-control" name="idEmpleadoEspecialidad">
            <?php foreach ($data['especialidades'] as $especialidad): ?>
                    <?php if($especialidad['id_empleado_especialidad'] == $data['empleado']->getIdEmpleadoEspecialidad() ): ?>
                        <option value="<?php echo $especialidad['id_empleado_especialidad']; ?>" selected="selected"><?php echo $especialidad['nombre_especialidad']; ?></option>
                    <?php else: ?>
                        <option value="<?php echo $especialidad['id_empleado_especialidad']; ?>"><?php echo $especialidad['nombre_especialidad']; ?></option>
                    <?php endif; ?>
            <?php endforeach; ?>
        </select>
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
</form>