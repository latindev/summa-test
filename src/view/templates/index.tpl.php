<?php
use summa\entity\Empresa;
/** @var Empresa $data['empresa'] */
/** @var \summa\entity\Empleado $data['empleado'] */

$search = (isset($data['search']) && !empty($data['search'])) ? $data['search'] : '';
?>

<div class="panel panel-default">
    <div class="panel-heading">
<h1>
    Listado de empleados de la empresa: <?php echo $data['empresa']->getNombre(); ?>
</h1>
    </div>
    <div class="panel-body">

<h4>Buscar empleados por id</h4>
<?php if(isset($data['error'])): ?>
<div class="alert alert-danger" role="alert">
    El id tiene que ser númerico y mayor a cero
</div>
<?php endif; ?>
<form class="form-inline" method="post">
    <div class="form-group">
        <input type="text" name="id_empleado" class="form-control" id="id_empleado" placeholder="id del empleado" value="<?php echo $search;?>">
    </div>
    <button type="submit" class="btn btn-default">Buscar</button>
</form>
<table class="table table-hover">
    <thead>
        <tr>
            <th>#id</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Edad</th>
            <th>Especialidad</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data['empresa']->getEmpleados() as $empleado): ?>
                <tr>
                    <td><?php echo $empleado->getId(); ?></td>
                    <td><?php echo $empleado->getNombre(); ?></td>
                    <td><?php echo $empleado->getApellido(); ?></td>
                    <td><?php echo $empleado->getEdad(); ?></td>
                    <td><?php echo $empleado->getNombreEspecialidad(); ?></td>
                </tr>
        <?php endforeach; ?>
    </tbody>
</table>
        </div>
    </div>
<div class="panel panel-default">
    <div class="panel-heading">
        Promedio de edad
    </div>
    <div class="panel-body">
        El promedio de edad de los trabajadores es de <?php echo $data['avg']; ?>
    </div>
</div>


