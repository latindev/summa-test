<?php
/**
 * User: David Fichtenbaum
 * Date: 23/04/16
 * Time: 12:41
 */

namespace summa\view;


class View
{
    private $pathTemplates;
    protected $data;
    private $templateFile;
    private $preOutput;

    /**
     * View constructor.
     */
    public function __construct()
    {
        $this->pathTemplates = __DIR__ . '/templates/';
    }

    /**
     * @param $templateFile
     * @throws \Exception
     */
    public function template($templateFile)
    {
        $templateFile = $this->pathTemplates . $templateFile;

        if (!file_exists($templateFile)) {
            throw new \Exception('No se encontro el template: ' . $templateFile );
        }

        $this->templateFile = $templateFile;

    }

    protected function preRender()
    {
        ob_start();
        include_once $this->pathTemplates . 'system/header.tpl.php';
        $this->preOutput['header'] = ob_get_contents();
        ob_clean();

        include_once $this->pathTemplates . 'system/footer.tpl.php';
        $this->preOutput['footer'] = ob_get_contents();
        ob_clean();
    }

    /**
     * @param array $data
     */
    public function render($data)
    {
        $this->preRender();

        //extract($data);

        ob_clean();
        ob_start();

        echo $this->preOutput['header'];
        include_once $this->templateFile;

        echo $this->preOutput['footer'];
        ob_flush();

        exit;
    }

    /**
     * @return mixed
     */
    public function getPreOutput()
    {
        return $this->preOutput;
    }

}