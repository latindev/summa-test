CREATE TABLE `empleado_especialidad` (
  `id_empleado_especialidad` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_tipo` int(10) unsigned NOT NULL,
  `nombre_especialidad` varchar(45) NOT NULL,
  PRIMARY KEY (`id_empleado_especialidad`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

CREATE TABLE `empleados` (
  `id_empleado` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `edad` tinyint(3) unsigned NOT NULL,
  `id_empleado_especialidad` int(10) unsigned NOT NULL,
  `id_empresa` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_empleado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `empresa` (
  `id_empresa` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`id_empresa`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

CREATE TABLE `tipos_empleado` (
  `id_tipo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre_tipo` varchar(45) NOT NULL,
  PRIMARY KEY (`id_tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;


INSERT INTO `empresa` (`nombre`) VALUES ('Acme Supplier');

INSERT INTO `tipos_empleado` (`nombre_tipo`) VALUES ('Programador');
INSERT INTO `tipos_empleado` (`nombre_tipo`) VALUES ('Diseniador');

INSERT INTO `empleado_especialidad` (`id_tipo`, `nombre_especialidad`) VALUES ('1', 'PHP');
INSERT INTO `empleado_especialidad` (`id_tipo`, `nombre_especialidad`) VALUES ('1', 'Python');
INSERT INTO `empleado_especialidad` (`id_tipo`, `nombre_especialidad`) VALUES ('1', 'NET');
INSERT INTO `empleado_especialidad` (`id_tipo`, `nombre_especialidad`) VALUES ('2', 'Web');
INSERT INTO `empleado_especialidad` (`id_tipo`, `nombre_especialidad`) VALUES ('2', 'Dise&ntilde;ador');
