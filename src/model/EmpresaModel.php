<?php
/**
 * User: David Fichtenbaum
 * Date: 21/04/16
 * Time: 15:21
 */

namespace summa\model;

use summa\database\MySQL;
use summa\entity\Diseniador;
use summa\entity\Empleado;
use summa\app\Config;
use summa\entity\Empresa;
use summa\entity\Programador;


class EmpresaModel
{

    /**
     * @var \PDO
     */
    private $db;

    /**
     * EmpresaModel constructor.
     *
     */
    public function __construct()
    {
        $this->db = new MySQL(
            Config::$user,
            Config::$password,
            Config::$db_name,
            Config::$hostname
        );
    }

    /**
     * @param \summa\entity\Empleado $empleado
     * @throws \Exception
     */
    public function insertEmpleado($empleado)
    {

        if (!$empleado instanceof Empleado) {
            throw new \Exception('No pertenece a un empleado');
        }

        $sth = 'INSERT INTO empleados (nombre, apellido, edad, id_empresa, id_empleado_especialidad) VALUES (:nombre, :apellido, :edad, :id_empresa, :id_empleado_especialidad )';
        $params = [
            ':nombre' => $empleado->getNombre(),
            ':apellido' => $empleado->getApellido(),
            ':edad' => $empleado->getEdad(),
            ':id_empresa' => $empleado->getIdEmpresa(),
            ':id_empleado_especialidad' => $empleado->getIdEmpleadoEspecialidad()
        ];

        try {

            $this->db->insert($sth, $params);
        } catch (\PDOException $ex) {
            throw new \Exception("no se pudo cargar el empleado " . $empleado->getNombre() . " " . $empleado->getApellido());
        }


    }

    /**
     * Obtiene las especialidades de un determinado tipo de empleado
     * @param $idTipo
     * @return array de especialidades
     */
    public function getEmpleadoEspecialidadesByTipoEmpleado($idTipo)
    {
        $sth = 'SELECT * FROM empleado_especialidad WHERE id_tipo = :id_tipo';
        $params = [
            ':id_tipo' => $idTipo
        ];

        $especialidades = $this->db->query($sth, $params);

        return $especialidades;
    }

    /**
     * @param $nombreTipo
     * @return array
     */
    public function getTipoEmpleado($nombreTipo)
    {
        $nombreTipo = explode('\\', $nombreTipo);
        $nombreTipo = $nombreTipo[count($nombreTipo) -1 ];
        $sth = 'SELECT * FROM tipos_empleado WHERE nombre_tipo = :nombre_tipo';
        $params = [
            ':nombre_tipo' => $nombreTipo
        ];

        try {
            $especialidades = $this->db->query($sth, $params);
            return $especialidades[0];
        } catch (\Exception $ex) {

        }

    }


    /**
     * Obtiene todos los empleados de una empresa
     *
     * @param \summa\entity\Empresa $empresa
     * @param array $filter
     * @return array de \summa\entity\Empleado
     */
    public function getAllEmpleados(Empresa $empresa, $filter = [])
    {

        $params = [
            ':id_empresa' => $empresa->getId(),
        ];

        $extendedCondition = '';
        if (count($filter) > 0 && isset($filter['id_empleado'])) {
            $extendedCondition = ' AND empl.id_empleado = :id_empleado';
            $params[':id_empleado'] = $filter['id_empleado'];
        }

        $sth = 'SELECT empl.*, t.*, ee.* FROM
    empleados AS empl
        INNER JOIN
    empleado_especialidad AS ee ON (empl.id_empleado_especialidad = ee.id_empleado_especialidad)
        INNER JOIN
    empresa emp ON (emp.id_empresa = empl.id_empresa)
        INNER JOIN
    tipos_empleado t ON (t.id_tipo = ee.id_tipo)
WHERE
    empl.id_empresa = :id_empresa ' . $extendedCondition;


        $empleados = [];

        try {
            $sthrst = $this->db->query($sth, $params);

            foreach ($sthrst as $itemEmpleado) {

                $empleadoType = 'summa\\entity\\' . $itemEmpleado['nombre_tipo'];

                /**
                 * @var Programador|Diseniador $empleado
                 */
                //$id, $nombre, $apellido, $edad, $idEmpresa, $idTipo, $nombreTipo, $idEmpleadoEspcialidad, $nombreEspecialidad
                $empleado = new $empleadoType(
                    $itemEmpleado['id_empleado'],
                    $itemEmpleado['nombre'],
                    $itemEmpleado['apellido'],
                    $itemEmpleado['edad'],
                    $itemEmpleado['id_empresa'],
                    $itemEmpleado['id_tipo'],
                    $itemEmpleado['nombre_tipo'],
                    $itemEmpleado['id_empleado_especialidad'],
                    $itemEmpleado['nombre_especialidad']
                );

                $empleados[] = $empleado;

            }

            return $empleados;

        } catch (\Exception $ex) {

        }

    }

    /**
     * @return Empresa
     */
    public function getEmpresa()
    {
        try{

            $sql = 'SELECT * FROM empresa';
            $result = $this->db->query($sql);
            $empresa = new Empresa();
            foreach ($result as $item) {
                $empresa->setId($item['id_empresa']);
                $empresa->setNombre($item['nombre']);
            }

            return $empresa;
        } catch (\Exception $ex) {

        }
    }

    /**
     * Retorna el promedio de edad de los trabajadores
     * @return string
     */
    public function getAgeAverage()
    {
        $sth = 'SELECT
    avg(empl.edad) as \'edadPromedio\'
FROM
    empleados AS empl
        INNER JOIN
    empleado_especialidad AS ee ON (empl.id_empleado_especialidad = ee.id_empleado_especialidad)
        INNER JOIN
    empresa emp ON (emp.id_empresa = empl.id_empresa)
        INNER JOIN
    tipos_empleado t ON (t.id_tipo = ee.id_tipo)
WHERE
    empl.id_empresa = :id_empresa';

        try {
            $empresa = $this->getEmpresa();
            $params = [':id_empresa' => $empresa->getId()];
            $avg =  $this->db->query($sth, $params);

            if(isset($avg[0]['edadPromedio'])) {
                return number_format((float)$avg[0]['edadPromedio'],2);

            } else {
                return '0';
            }
        } catch (\Exception $ex) {

        }

    }

}