<?php
/**
 * User: David Fichtenbaum
 * Date: 20/04/16
 * Time: 23:13
 */

namespace summa\entity;

abstract class Empleado
{
    private $idEmpresa;
    /**
     * @type int
     */
    private $id;
    /**
     * @type string
     */
    private $nombre;
    /**
     * @type string
     */
    private $apellido;
    /**
     * @type int
     */
    private $edad;

    /**
     * @var int
     */
    private $idTipo;

    /**
     * @type string
     */
    private $nombreTipo;

    /**
     * @type int
     */
    private $idEmpleadoEspecialidad;

    /**
     * @type string
     */
    private $nombreEspecialidad;

    /**
    * @var
    */
    private $idEmpleadoEspcialidad;


    /**
     * Empleado constructor.
     *
     * @param $id
     * @param $nombre
     * @param $apellido
     * @param $edad
     * @param $idEmpresa
     * @param $idTipo
     * @param $nombreTipo
     * @param $idEmpleadoEspcialidad
     * @param $nombreEspecialidad
     */
    public function __construct($id, $nombre, $apellido, $edad, $idEmpresa, $idTipo, $nombreTipo, $idEmpleadoEspcialidad, $nombreEspecialidad)
    {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->apellido = $apellido;
        $this->edad = $edad;
        $this->idEmpresa = $idEmpresa;
        $this->idTipo = $idTipo;
        $this->nombreTipo = $nombreTipo;
        $this->idEmpleadoEspcialidad = $idEmpleadoEspcialidad;
        $this->nombreEspecialidad = $nombreEspecialidad;
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * @param string $apellido
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;
    }

    /**
     * @return int
     */
    public function getEdad()
    {
        return $this->edad;
    }

    /**
     * @param int $edad
     */
    public function setEdad($edad)
    {
        $this->edad = $edad;
    }

    /**
     * @return int
     */
    public function getIdEmpresa()
    {
        return $this->idEmpresa;
    }

    /**
     * @param int $idEmpresa
     */
    public function setIdEmpresa($idEmpresa)
    {
        $this->idEmpresa = $idEmpresa;
    }

    /**
     * @return int
     */
    public function getIdTipo()
    {
        return $this->idTipo;
    }

    /**
     * @param int $idTipo
     */
    public function setIdTipo($idTipo)
    {
        $this->idTipo = $idTipo;
    }

    /**
     * @return string
     */
    public function getNombreTipo()
    {
        return $this->nombreTipo;
    }

    /**
     * @param string $nombreTipo
     */
    public function setNombreTipo($nombreTipo)
    {
        $this->nombreTipo = $nombreTipo;
    }

    /**
     * @return int
     */
    public function getIdEmpleadoEspecialidad()
    {
        return $this->idEmpleadoEspecialidad;
    }

    /**
     * @param int $idEmpleadoEspecialidad
     */
    public function setIdEmpleadoEspecialidad($idEmpleadoEspecialidad)
    {
        $this->idEmpleadoEspecialidad = $idEmpleadoEspecialidad;
    }

    /**
     * @return mixed
     */
    public function getNombreEspecialidad()
    {
        return $this->nombreEspecialidad;
    }

    /**
     * @param mixed $nombreEspecialidad
     */
    public function setNombreEspecialidad($nombreEspecialidad)
    {
        $this->nombreEspecialidad = $nombreEspecialidad;
    }

}