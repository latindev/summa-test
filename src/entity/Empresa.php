<?php
/**
 * User: David Fichtenbaum
 * Date: 22/04/16
 * Time: 7:31
 */

namespace summa\entity;

class Empresa
{
    private $id;
    private $nombre;

    /**
     * @type array Empleado
     */
    private $empleados;


    /**
     * Empleados constructor.
     */
    public function __construct()
    {
        $this->empleados = [];
    }

    /**
    * @return mixed
    */
    public function getId()
    {
        return $this->id;
    }

    /**
    * @param mixed $id
    */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
    * @return mixed
    */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
    * @param mixed $nombre
    */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }


    /**
     * @return mixed
     */
    public function getEmpleados()
    {
        return $this->empleados;
    }

    /**
     * @param array $empleados
     */
    public function setEmpleados($empleados)
    {
        $this->empleados = $empleados;
    }

}