<?php
/**
 * User: David Fichtenbaum
 * Date: 20/04/16
 * Time: 23:58
 */

namespace summa\entity;

class Diseniador extends Empleado
{

    /**
     * Diseniador constructor.
     *
     * @param int $id
     * @param string $nombre
     * @param string $apellido
     * @param int $edad
     * @param int $idEmpresa
     * @param int $idTipo
     * @param $nombreTipo
     * @param $idEmpleadoEspcialidad
     * @param $nombreEspcialidad
     */
    public function __construct($id, $nombre, $apellido, $edad, $idEmpresa, $idTipo, $nombreTipo, $idEmpleadoEspcialidad, $nombreEspcialidad)
    {
        parent::__construct($id, $nombre, $apellido, $edad, $idEmpresa, $idTipo, $nombreTipo, $idEmpleadoEspcialidad, $nombreEspcialidad);

    }

}