<?php
/**
 * User: David Fichtenbaum
 * Date: 20/04/16
 * Time: 23:28
 */

namespace summa\entity;

class Programador extends Empleado
{

    /**
     * Programador constructor.
     *
     * @param $id
     * @param $nombre
     * @param $apellido
     * @param $edad
     * @param $idEmpresa
     * @param $idTipo
     * @param $nombreTipo
     * @param $idEmpleadoEspcialidad
     * @param $nombreEspcialidad
     */
    public function __construct($id, $nombre, $apellido, $edad, $idEmpresa, $idTipo, $nombreTipo, $idEmpleadoEspcialidad, $nombreEspcialidad)
    {
        parent::__construct($id, $nombre, $apellido, $edad, $idEmpresa, $idTipo, $nombreTipo, $idEmpleadoEspcialidad, $nombreEspcialidad);
    }

}