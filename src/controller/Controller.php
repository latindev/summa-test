<?php
/**
 * User: David Fichtenbaum
 * Date: 23/04/16
 * Time: 11:32
 */

namespace summa\controller;


use summa\view\View;

abstract class Controller
{

    /**
     * @type View
     */
    protected $view;

    protected $dataView;

    public function __construct()
    {
        $this->view = new View();
    }

    public function render()
    {
        $this->view->render($this->dataView);
    }
}