<?php
/**
 * User: David Fichtenbaum
 * Date: 20/04/16
 * Time: 23:09
 */

namespace summa\controller;

use summa\entity\Diseniador;
use summa\entity\Empresa;
use summa\entity\Programador;
use summa\model\EmpresaModel;

class EmpresaController extends Controller
{

    /** @type  Empresa */
    private $empresaModel;

    /**
     * EmpresaController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->empresaModel = new EmpresaModel();
        $this->dataView['empresa'] = $this->empresaModel->getEmpresa();
    }

    /**
     * Home
     */
    public function index()
    {
        $filter = [];
        if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] == 'POST') {
            $filter['id_empleado'] = (isset($_POST['id_empleado'])) ? (int) $_POST['id_empleado'] : 0;
            if ($filter['id_empleado'] == 0) {
                $this->dataView['error'] = true;
                $filter = [];
            } else {
                $this->dataView['search'] = $filter['id_empleado'];
            }

        }
        $this->dataView['empresa']->setEmpleados($this->empresaModel->getAllEmpleados($this->dataView['empresa'], $filter));
        $this->dataView['avg'] = $this->empresaModel->getAgeAverage();

        $this->view->template('index.tpl.php');
        $this->render();
    }

    /**
     * Agrego un emprleado del tipo programador
     * @throws \Exception
     */
    public function addEmpleadoProgramador()
    {
        $tipo_empleado = $this->empresaModel->getTipoEmpleado(Programador::class);
        $empleadoProgramador = new Programador(0,'','','',$this->dataView['empresa']->getId(),$tipo_empleado['id_tipo'],$tipo_empleado['nombre_tipo'],'','');

        $this->dataView['especialidades'] = $this->empresaModel->getEmpleadoEspecialidadesByTipoEmpleado($empleadoProgramador->getIdTipo());

        if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] == 'POST') {
            $datosEmpleado = $this->validarEmpleado();

            $empleadoProgramador->setNombre($datosEmpleado['post']['nombre']);
            $empleadoProgramador->setApellido($datosEmpleado['post']['apellido']);
            $empleadoProgramador->setEdad($datosEmpleado['post']['edad']);
            $empleadoProgramador->setIdEmpleadoEspecialidad($datosEmpleado['post']['idEmpleadoEspecialidad']);


            if (count($datosEmpleado['errores']) > 0 ) {
                $this->dataView['errores'] = $datosEmpleado['errores'];

            } else {
                $this->empresaModel->insertEmpleado($empleadoProgramador);
                $this->dataView['success'] = true;
                $empleadoProgramador = new Programador(0,'','','',$this->dataView['empresa']->getId(),$tipo_empleado['id_tipo'],$tipo_empleado['nombre_tipo'],'','');
            }

        }

        $this->dataView['empleado'] = $empleadoProgramador;

        $this->view->template('agregarEmpleadoProgramador.tpl.php');
        $this->render();
    }

    /**
     * Agrego un empleado del tipo diseñador
     * @throws \Exception
     */
    public function addEmpleadoDesign()
    {
        $tipo_empleado = $this->empresaModel->getTipoEmpleado(Diseniador::class);
        $empleadoDesign = new Diseniador(0,'','','',$this->dataView['empresa']->getId(),$tipo_empleado['id_tipo'],$tipo_empleado['nombre_tipo'],'','');

        $this->dataView['especialidades'] = $this->empresaModel->getEmpleadoEspecialidadesByTipoEmpleado($empleadoDesign->getIdTipo());

        if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] == 'POST') {
            $datosEmpleado = $this->validarEmpleado();

            $empleadoDesign->setNombre($datosEmpleado['post']['nombre']);
            $empleadoDesign->setApellido($datosEmpleado['post']['apellido']);
            $empleadoDesign->setEdad($datosEmpleado['post']['edad']);
            $empleadoDesign->setIdEmpleadoEspecialidad($datosEmpleado['post']['idEmpleadoEspecialidad']);

            if (count($datosEmpleado['errores']) > 0 ) {
                $this->dataView['errores'] = $datosEmpleado['errores'];

            } else {
                $this->empresaModel->insertEmpleado($empleadoDesign);
                $this->dataView['success'] = true;
                $empleadoDesign = new Diseniador(0,'','','',$this->dataView['empresa']->getId(),$tipo_empleado['id_tipo'],$tipo_empleado['nombre_tipo'],'','');
            }

        }

        $this->dataView['empleado'] = $empleadoDesign;

        $this->view->template('agregarEmpleadoDesign.tpl.php');
        $this->render();
    }

    /**
     * Valida los campos de un empleado
     * @return array
     */
    private function validarEmpleado()
    {
        $data['errores'] = [];
        $nombre = (isset($_POST['nombre'])) ? trim($_POST['nombre']) : '';
        $apellido = (isset($_POST['apellido'])) ? trim($_POST['apellido']) : '';
        $edad = (isset($_POST['edad'])) ? (int) trim($_POST['edad']) : '';
        $idEmpleadoEspecialidad = (isset($_POST['idEmpleadoEspecialidad'])) ? trim($_POST['idEmpleadoEspecialidad']) : '';

        if (empty($nombre)) {
            array_push($data['errores'], 'El nombre es obligatorio');
        }

        if (empty($apellido)) {
            array_push($data['errores'], 'El apellido es obligatorio');
        }

        if (empty($edad)) {
            array_push($data['errores'], 'La edad es obligatoria');

        } elseif ($edad <= 0) {
            $edad = '';
            array_push($data['errores'], 'La edad debe ser númerica y mayor a cero ');
        }

        $data['post'] = [
            'nombre' => $nombre,
            'apellido' => $apellido,
            'edad' => $edad,
            'idEmpleadoEspecialidad' => $idEmpleadoEspecialidad
        ];

        return $data;
    }

    public function getAgeAverage()
    {

        $this->view->template('getAgeAverage.tpl.php');
        $this->render();

    }
}